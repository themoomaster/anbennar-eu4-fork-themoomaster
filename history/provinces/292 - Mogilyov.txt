#292 - Mogilyov

owner = A33
controller = A33
add_core = A33
culture = vernman
religion = regent_court

hre = yes

base_tax = 5
base_production = 6
base_manpower = 6

trade_goods = copper

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_gnollish
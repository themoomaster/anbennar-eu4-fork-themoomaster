#650 - Gorihradin (mountaincastle)

owner = F13
controller = F13
add_core = F13
add_core = F20
culture = exodus_goblin
religion = goblinic_shamanism


base_tax = 3
base_production = 2
base_manpower = 1

trade_goods = iron

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari
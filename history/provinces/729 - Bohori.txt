# No previous file for Bohori
culture = green_orc
religion = great_dookan
capital = ""

hre = no

base_tax = 3
base_production = 3
base_manpower = 3

trade_goods = unknown

native_size = 30
native_ferocity = 9
native_hostileness = 8
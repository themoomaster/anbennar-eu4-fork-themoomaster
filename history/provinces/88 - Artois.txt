#85 - Artois | 

owner = A09
controller = A09
add_core = A09
culture = sorncosti
religion = regent_court
hre = no
base_tax = 7
base_production = 7
trade_goods = wine
base_manpower = 5
capital = "Home of the Sornc�st Vintage"
is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

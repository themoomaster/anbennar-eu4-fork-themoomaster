#53 - L�neburg

owner = A31
controller = A31
add_core = A31
culture = moon_elf
religion = regent_court

hre = yes

base_tax = 7
base_production = 7
base_manpower = 4

trade_goods = glass

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish


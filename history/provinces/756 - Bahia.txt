# No previous file for Bahia
culture = green_orc
religion = great_dookan
capital = ""

hre = no

base_tax = 4
base_production = 3
base_manpower = 3

trade_goods = fish

native_size = 43
native_ferocity = 6
native_hostileness = 7
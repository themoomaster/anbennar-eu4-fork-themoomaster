# No previous file for Catamarca
culture = green_orc
religion = great_dookan
capital = ""

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = wool

native_size = 14
native_ferocity = 7
native_hostileness = 7
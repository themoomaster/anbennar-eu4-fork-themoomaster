# No previous file for Bali
owner = F31
controller = F31
add_core = F31
culture = zanite
religion = bulwari_sun_cult

hre = no

base_tax = 5
base_production = 4
base_manpower = 2

trade_goods = incense

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
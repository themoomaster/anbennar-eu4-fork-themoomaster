government = merchant_republic
government_rank = 1
primary_culture = bluefoot_halfling
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 66
fixed_capital = 66 # Cannot move capital away from this province & no power cost to move to it

1440.2.2 = {
	monarch = {
		name = "Darian"
		dynasty = "Longfellow"
		birth_date = 1399.12.12
		adm = 1
		dip = 3
		mil = 1
	}
}
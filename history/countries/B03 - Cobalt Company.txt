government = adventurer
government_rank = 1
primary_culture = gawedi
religion = regent_court
technology_group = tech_cannorian
capital = 750
historical_rival = B04 #Lorentish Ashen Rose

1443.10.5 = {
	monarch = {
		name = "Gunther"
		birth_date = 1387.3.8
		adm = 2
		dip = 1
		mil = 5
	}
}
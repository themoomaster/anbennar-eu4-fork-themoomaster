government = feudal_monarchy
government_rank = 1
primary_culture = vernman
religion = regent_court
technology_group = tech_cannorian
capital = 295
national_focus = DIP

1421.4.7 = {
	monarch = {
		name = "Adrien IV"
		dynasty = "s�l Gal�inn"
		birth_date = 1412.11.11
		adm = 3
		dip = 1
		mil = 2
	}
}

1422.1.1 = { set_country_flag = lilac_wars_moon_party }
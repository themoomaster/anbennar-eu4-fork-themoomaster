government = merchant_republic
government_rank = 1
primary_culture = brasanni
add_accepted_culture = sun_elf
religion = bulwari_sun_cult
technology_group = tech_bulwari
capital = 565

1440.1.1 = {
	monarch = {
		name = "City Council"
		adm = 2
		dip = 2
		mil = 2
		regent = yes
	}
}
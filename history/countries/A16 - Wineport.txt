government = merchant_republic #wine baron council
government_rank = 1
mercantilism = 25
primary_culture = high_lorentish
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 101

1428.3.18 = {
	monarch = {
		name = "�nri the Taster"
		birth_date = 1410.4.9
		adm = 2
		dip = 2
		mil = 2
	}
}